# go-hello

example project

## Usage

```go
hello.HelloWorld()
```

## Requirements

* golang

## Installation

```
$ go get gitlab.com/mattn/go-hello
```

## License

MIT

## Author

Yasuhiro Matsuhiro (a.k.a mattn)
